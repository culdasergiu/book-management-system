create database bookmanagement_db;
use bookmanagement_db;
CREATE TABLE author(id int primary key auto_increment, firstName varchar(20) not null, lastName varchar(20) not null);

insert into author(firstName, lastName) values("John","Fowells");
insert into author(firstName, lastName) values ("Ion","Creanga"),("Mihai","Eminescu"), ("Lucian","Blaga"),("Jane","Austin"),("John","Fowells");

select * from author;

create table book(id int primary key auto_increment, title varchar(30) not null, bookDescription text, author int not null , 
CONSTRAINT fk_author
 FOREIGN KEY (author) REFERENCES author (id));
 drop table book;
 
 select * from author, book;
 
insert into book(title, bookDescription, author) values ("Pride and Prejudice" , "Pride and Prejudice is a
romantic novel of manners written by Jane Austen in 1813. The novel follows the character development of
Elizabeth Bennet, the dynamic protagonist of the book who learns about the repercussions of hasty judgments
and comes to appreciate the difference between superficial goodness and actual goodness. Its humour lies in its
honest depiction of manners, education, marriage, and money during the Regency era in Great Britain.", 2);
insert into book(title, bookDescription, author) values ("Emma", "Emma, by Jane Austen, is a novel about youthful
hubris and romantic misunderstandings. It is set in the fictional country village of Highbury and the surrounding
estates of Hartfield, Randalls and Donwell Abbey, and involves the relationships among people from a small number of
families.[2] The novel was first published in December 1815, with its title page listing a publication date of 1816.
As in her other novels, Austen explores the concerns and difficulties of genteel women living in Georgian–Regency England.
Emma is a comedy of manners, and depicts issues of marriage, sex, age, and social status.", 2);
insert into book(title, bookDescription, author) values ("Sanditon" , "Sanditon (1817) is an unfinished novel by the English writer
 John Fowells. In January 1817, John began work on a new novel he called The Brothers, later titled Sanditon, and
 completed eleven chapters before stopping work in mid-March 1817, probably because of his illness.[1] R.W.
 Chapman first published a full transcription of the novel in 1925 under the name Fragment of a Novel.[2]", 1);
 
 create table reviews(id INT not null auto_increment primary key,
title varchar(50) not null,
score float(50) not null,
comments varchar(300) not null,
idBook int not null,
constraint fk_review foreign key (idBook) references book(id));
insert into reviews (title, score, comments, idBook) values ("Emma", 8.6, "Great book", 2);
insert into reviews (title, score, comments, idBook) values ("Pride and Prejudice", 9.2, "Excelent book", 1);

select * from reviews;

 
 
 