import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class AuthorRepository {

    // Method to add an author record in the database
    public static List<Author> findAll() {
        Session session = HibernateUtils.getSessionFactory().openSession();

        String selectAllAuthorsHQL = "from Author";
        Query selectAllAuthorsQuery = session.createQuery(selectAllAuthorsHQL);
        List<Author> allAuthors = selectAllAuthorsQuery.list();

        session.close();

        return allAuthors;
    }

    public void add(String firstName, String lastName) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Author author = new Author(firstName, lastName);

        session.save(author);
        transaction.commit();
        session.close();
    }

    public void update(int id, String firstName, String lastName) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Author author = session.get(Author.class, id);
        author.setFirstName(firstName);
        author.setLastName(lastName);

        session.update(author);
        transaction.commit();
        session.close();

    }

    public void delete(Integer id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Author author = session.get(Author.class, id);

        session.delete(author);
        transaction.commit();
        session.close();
    }
}
