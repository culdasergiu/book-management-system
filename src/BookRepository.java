import org.hibernate.*;

import java.util.ArrayList;
import java.util.List;

public class BookRepository {

    //allows the viewing of all the books
    public static List<Book> findAllBooks() {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        if (sessionFactory == null){
            System.out.println("SessionFactory is null");
            return new ArrayList<>();
        }
        Session session = HibernateUtils.getSessionFactory().openSession();
        String selectAllBooksHql = "from Book";
        Query selectAllBooksQuery = session.createQuery(selectAllBooksHql);
        List<Book> allBooks = selectAllBooksQuery.list();
        session.close();
        return allBooks;
    }

    //adding a new book - and assigning one of the existing authors
    public void addBook(String title, String description, int author) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Author author1 = new Author();
        author1.setId(author);
        Book book = new Book();
        book.setTitle(title);
        book.setDescription(description);
        book.setAuthor(author1);
        session.save(book);
        transaction.commit();
        session.close();
    }

    //deleting an existing book
    public void deleteBook(int idBook) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Book book = session.get(Book.class, idBook);
        session.delete(book);
        transaction.commit();
        session.close();
    }

    //updating an existing book
    public void updateBook(int idBook, String description) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Book book = session.get(Book.class, idBook);
        book.setDescription(description);
        transaction.commit();
        session.close();
    }
}
