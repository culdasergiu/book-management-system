import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        //calling method for getting all authors
        List<Author> authorsList = AuthorRepository.findAll();
        for (Author author : authorsList) {
            System.out.println(author);
        }

        //calling method for getting all reviews
        List<Reviews> listRev = ReviewUtils.findAllReviews();
        for (Reviews rev : listRev) {
            System.out.println("-------------------------------------");
            System.out.println(rev.toString());
        }

        //Adding a new author calling method
        AuthorRepository author = new AuthorRepository();

//        author.add("Jane","Austen");
//        author.add("John", "Fowells");
//        author.add("Gabriel Garcia", "Marquez");

        //deleting authors by id calling method
//        author.delete(7);

        //updating authors calling method
//        author.update(3, "Daly", "King");


    }
}
