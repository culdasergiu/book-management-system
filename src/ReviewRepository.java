import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class ReviewRepository {

    //allows the viewing of all the reviews
    public static List<Reviews> findAllReviews() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            String selectAllReviewsHQL = "from Reviews";
            Query selectAllReviewsQuery = session.createQuery(selectAllReviewsHQL);
            List<Reviews> reviews = selectAllReviewsQuery.list();
            session.close();
            return reviews;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void addReview(String title, double score, String comment, int idBook) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Reviews reviews = new Reviews();
        reviews.setBookName(title);
        reviews.setScore(score);
        reviews.setComments(comment);
        Book book = new Book();
        book.setId(idBook);
        reviews.setIdBook(book);
        //reviews.setId(idBook);
        session.save(reviews);
        transaction.commit();
        session.close();
    }

    //updating an existing review
    public void updateReviews(int idBook, String comment, double score){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Reviews reviews = session.get(Reviews.class, idBook);
        reviews.setScore(score);
        reviews.setComments(comment);
        transaction.commit();
        session.close();

    }

    //deleting an existing review
    public void deleteReviews(int id){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Reviews reviews = session.get(Reviews.class, id);
        session.delete(reviews);
        transaction.commit();
        session.close();
    }
}