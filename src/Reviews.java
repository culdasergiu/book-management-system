import javax.persistence.*;

@Entity
@Table(name = "reviews")
public class Reviews {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "score")
    private double score;
    @Column(name = "comments")
    private String comments;

//    public Reviews(int id, String title, double score, String comments, Book book) {
//        this.id = id;
//        this.title = title;
//        this.score = score;
//        this.comments = comments;
//        this.book = book;
//    }

    @ManyToOne
    @JoinColumn(name = "idBook")
    private Book book;

    public void setBookName(String title) {
        this.title = title;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public void setIdBook(Book book) {
        this.book = book;
    }

    public void setId(Book book) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getBookName() {
        return title;
    }

    public double getScore() {
        return score;
    }

    public String getComments() {
        return comments;
    }

    @Override
    public String toString() {
        return "Review: " +  id + " " + title + " " +  score  + " " +  comments;
    }

}
